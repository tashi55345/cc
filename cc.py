# -*- coding: utf-8 -*-
from coincheck import order, market, account
import requests
import json


""" 仮想通貨の現在所持している総資産を表示 """
def main():
    cc = ["btc", "eth", "etc", "dao", "lsk", "fct", "xmr", "rep", "xrp", "zec","xem","ltc","dash"]
    jpy = 0
    a = account.Account(secret_key="",access_key="")
    """ アカウントの各通貨の残高を確認 """
    myba = a.get_balance()
    for c in cc:
        r = requests.get("https://coincheck.com/api/rate/" + c + "_jpy" )
        item = r.json()
        rate = item['rate']
        jpy = jpy + float(rate) * float(myba[c])
    print("総資産: " + str(jpy) + "円")

if __name__ == '__main__':
    main()
